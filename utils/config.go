package utils

type Config struct {
	ProjectKey       string
	PropertyGroupKey string
	VersionKey       string
	Extension        string
}

func GetConfig() Config {
	config := Config{
		ProjectKey:       "Project",
		PropertyGroupKey: "PropertyGroup",
		VersionKey:       "Version",
		Extension:        ".csproj",
	}

	return config
}
