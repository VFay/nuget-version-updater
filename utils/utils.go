package utils

import (
	"io/fs"
	"path/filepath"
)

func FindFiles(config Config, rootPath string) []string {
	var files []string
	filepath.WalkDir(rootPath, func(s string, d fs.DirEntry, e error) error {
		if e != nil {
			return e
		}

		if filepath.Ext(d.Name()) == config.Extension {
			files = append(files, s)
		}

		return nil
	})
	return files
}
