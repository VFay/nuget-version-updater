package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/VFay/nuget-version-updater/updater"
	"gitlab.com/VFay/nuget-version-updater/utils"
)

func main() {
	version := flag.String("v", "", "")
	rootPath := flag.String("p", "", "")
	flag.Parse()

	if *version == "" {
		panic("Version was not provided")
	}

	if *rootPath == "" {
		ex, err := os.Executable()

		if err != nil {
			panic(err)
		}

		*rootPath = filepath.Dir(ex)
	}

	config := utils.GetConfig()

	files := utils.FindFiles(config, *rootPath)

	updater.UpdateNugetVersion(files, *version, config)

	fmt.Printf("Nuget Package Version updated for %v files \n", len(files))
}
