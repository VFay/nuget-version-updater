package updater

import (
	"github.com/beevik/etree"

	"gitlab.com/VFay/nuget-version-updater/utils"
)

func UpdateNugetVersion(files []string, version string, config utils.Config) {
	for i := range files {
		doc := etree.NewDocument()

		if err := doc.ReadFromFile(files[i]); err != nil {
			panic(err)
		}

		if root := doc.SelectElement(config.ProjectKey); root != nil {
			for _, propertyGroup := range root.SelectElements(config.PropertyGroupKey) {
				if versionProperty := propertyGroup.SelectElement(config.VersionKey); versionProperty != nil {
					versionProperty.SetText(version)
				}
			}
		}

		doc.WriteToFile(files[i])
	}
}
